from __future__ import division, absolute_import, unicode_literals
from scrapy import Request, Spider, FormRequest
import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError

import json
import traceback
import time
import os


class NexusPayablesSpider(Spider):
    name = "nexuspayables"

    allowed_domains = [
        'ap.nexuspayables.com',
        'www.intacct.com'
    ]
    start_urls = [
        'https://ap.nexuspayables.com/RESIDENTIALONE/login.php'
    ]

    INVOICE_API_URL='https://ap.nexuspayables.com/RESIDENTIALONE/ajax.php' \
                    '?_dc={dc}&service=InvoiceService' \
                    '&action=getInvoiceRegister&tab=Transferred' \
                    '&userprofile_id=503' \
                    '&delegated_to_userprofile_id=503' \
                    '&contextType=all&contextSelection=385' \
                    '&_origin=Invoice%3AshowRegister%3Atransferred&page={page_num}' \
                    '&start={page_start}&pageSize=200&sort=invoice_ref%20DESC'

    PDF_LINK = 'https://ap.nexuspayables.com/RESIDENTIALONE/showImage.php?ID={}'
    passed_invoices = []
    logs = {}

    def __init__(self, username=None, password=None, download_directory=None, *args, **kwargs):
        super(NexusPayablesSpider, self).__init__(*args, **kwargs)
        self.user_name = username
        self.password = password
        self.download_directory = download_directory if download_directory else 'C:/Users/Dev/Downloads/nexuspayables/'

        with open('scrapy.log', 'rb') as f:
            try:
                self.logs = json.load(f)
            except:
                self.logs = {}
            f.close()

    def parse(self, response):
        if not os.path.exists(self.download_directory):
            os.makedirs(self.download_directory)
        return FormRequest.from_response(response,formdata={
            'username': self.user_name,
            'pwd': self.password
        }, callback=self.after_login)

    def after_login(self, response):
        dc = self.current_timestamp()
        meta = response.meta.copy()
        page_num = meta.get('page_num', 1)
        page_start = meta.get('page_start', 0)
        return Request(
            self.INVOICE_API_URL.format(dc=dc, page_num=page_num, page_start=page_start),
            callback=self.invoice_page,
            dont_filter=True
        )

    def invoice_page(self, response):
        meta = response.meta.copy()
        page_num = meta.get('page_num', 1)
        page_start = meta.get('page_start', 0)
        try:
            page_num += 1
            page_start += 200
            data = json.loads(response.body)
            invoices = data.get('data', [])
            for inv in invoices:
                invoice_id = inv.get('invoice_id')
                invoice_ref = inv.get('invoice_ref')
                if invoice_ref not in self.logs:
                    invoice_date = inv.get('invoice_datetm', '').strip()
                    item = {
                        'Vendor': inv.get('vendor_name'),
                        'Amount': inv.get('control_amount'),
                        'Property Name': inv.get('property_name'),
                        'Invoice Number': inv.get('invoice_ref'),
                        'Invoice Date': self.date_to_string(invoice_date),
                        'Due Date': self.date_to_string(inv.get('invoice_duedate')),
                        'Post Date': self.date_to_string(inv.get('invoice_period')),
                        'Priority': inv.get('PriorityFlag_Display'),
                        'Status': inv.get('Integration_Package_Type_Display_Name')
                    }
                    dc = self.current_timestamp()
                    url = 'https://ap.nexuspayables.com/RESIDENTIALONE/ajax.php?_dc={dc}&_origin=Invoice%3AshowView%3A{invoice_id}' \
                           '&config=%5B%7B%22service%22%3A%22InvoiceService' \
                           '%22%2C%22action%22%3A%22get%22%2C%22invoice_id' \
                           '%22%3A%22{invoice_id}%22%7D%5D'.format(dc=dc, invoice_id=invoice_id)
                    yield Request(
                        url,
                        callback=self.before_download_page,
                        dont_filter=True,
                        meta={
                            'item': item,
                            'invoice_id': invoice_id,
                            'invoice_date': self.date_to_searchstr(invoice_date)
                        }
                    )
            if page_start < data.get('total'):
                dc = self.current_timestamp()
                yield Request(
                    self.INVOICE_API_URL.format(dc=dc, page_start=page_start, page_num=page_num),
                    dont_filter=True,
                    callback=self.invoice_page,
                    meta={
                        'page_start': page_start,
                        'page_num':page_num
                    }
                )

        except:
            self.logger.error('Error in Parsing Invoice JSON: {}'.format(traceback.format_exc()))

    def before_download_page(self, response):
        try:
            data = json.loads(response.body)
            invoice_detail = data[0]
            pdf_id = invoice_detail.get('image', {}).get('image_index_GUID')
            return Request(
                url=self.PDF_LINK.format(pdf_id),
                callback=self.download_page,
                dont_filter=True,
                meta=response.meta
            )
        except:
            self.logger.error('Error in parsing Invoice detail JSON: {}'.format(traceback.format_exc()))

    def download_page(self, response):
        meta = response.meta.copy()
        item = meta.get('item')
        invoice_id = meta.get('invoice_id')

        file_name = 'p{} {} {}.pdf'.format(item.get('Invoice Number'), item.get('Vendor', ''), item.get('Post Date')).replace('/', '-')

        file_link = '{}{}'.format(self.download_directory, file_name)
        with open(file_link, 'wb') as f:
            f.write(response.body)
            f.close()

        file_link = self.file_upload(file_name, response.body, invoice_id)
        item['file_link'] = file_link
        self.write_logs(item.get('Invoice Number'), {
            'file_link': file_link,
            'vendor_name': item.get('Vendor', ''),
            'bill_date': meta.get('invoice_date')
        })
        return item

    def file_upload(self, file_name, content, invoice_id):
        try:
            dbx = dropbox.Dropbox('KbtzgJvTpBAAAAAAAAAHiyMi-q0_HpA630moC70bLlTSLNPgV9NrKXmzKAWJP9r-')
            res = dbx.files_upload(content, '{}{}'.format(self.settings.get('UPLOAD_DIRECTORY'), file_name), mode=WriteMode('overwrite'))
            file_link = dbx.sharing_create_shared_link(res.path_display)
            return file_link.url
        except ApiError as err:
            self.logger.err('Error in uploading file to dropbox: {}'.format(err))
            return 'Invalid Link'

    def date_to_string(self, d):
        d = d.split(' ')
        if d:
            d = d[0].split('-')
            return ''.join([i.zfill(2) for i in d])
        return ''

    def date_to_searchstr(self, d):
        d = d.split(' ')
        if not d:
            return ''
        d = d[0].split('-')
        if len(d) != 3:
            return ''
        return d[1].zfill(2) + '/' + d[2].zfill(2) + '/' + d[0].zfill(2)


    def current_timestamp(self):
        return int(round(time.time() * 1000))

    def write_logs(self, bill_id, link):
        self.logs[str(bill_id)] = link
        with open('scrapy.log', 'w') as f:
            json.dump(self.logs, f)
        f.close()
