from selenium import webdriver
from time import sleep
import os
import logging
import json
from selenium.webdriver.common.action_chains import ActionChains

class Intacct:

    driver = None
    INTACCT_URL = 'https://www.intacct.com/ia/acct/login.phtml' \
                  '?.cpaassoc=1%40118543&.affiliation=' \
                  '&.fl=1&.theme=custom%3Bffffff%3B000000' \
                  '&.cpalogo=H5fEo_vc_FVIA6hNNzZMtLg0LHQJTJUUSYD2s2yzGLc..gif' \
                  '&.company=._%21Itwnub_SPiru' \
                  '&.login=nexusURL&.done=frameset.phtml'

    logs = {}
    info = {}
    items = {}

    def init_chrome(self):
        cwd = os.getcwd()
        self.driver = webdriver.Chrome(executable_path='{}/chromedriver.exe'.format(cwd))

    def update_intacct(self):
        if not self.driver:
            self.init_chrome()

        self.driver.get(self.INTACCT_URL)

        with open('cookie.log', 'r') as f:
            try:
                cookies = json.load(f)
            except:
                cookies = []
            for cookie in cookies:
                self.driver.add_cookie(cookie)
            f.close()

        password = self.find_element_by_id("passwd")
        password.send_keys('dz98F$29n!#tXUV')
        self.find_element_by_id('retbutton').click()

        # waiting for user's verfication code
        btn_bills = self.find_element_by_xpath(
            '//div[@class="iatopmenu"]//div[@class="iamenuinnerwrapper"]//span[@class="iamenuitem" and text()="Bills"]'
        )

        menu = self.find_element_by_xpath('//span[@class="iamenutitle" and contains(text(), "Accounts Payable")]')

        # store cookies to cookie.log
        with open('cookie.log', 'w') as f:
            json.dump(self.driver.get_cookies(), f)
            f.close()

        ActionChains(self.driver).move_to_element(menu).pause(5).click(btn_bills).perform()

        iframe = self.find_element_by_xpath('//iframe[@name="iamain"]')
        self.driver.switch_to.frame(iframe)

        for key in self.items:
            inp_vendor = self.find_element_by_xpath('//input[@name="F_VENDORNAME"]')
            inp_vendor.clear()
            inp_vendor.send_keys(self.items[key]['vendor_name'])

            inp_date = self.find_element_by_xpath('//input[@name="F_WHENCREATED"]')
            inp_date.clear()
            inp_date.send_keys(self.items[key]['bill_date'])

            inp_bills = self.find_element_by_xpath('//input[@name="F_RECORDID"]')
            inp_bills.clear()
            inp_bills.send_keys(key)

            btn_go = self.find_element_by_xpath('//input[@name="F_RECORDID"]/following-sibling::a')
            btn_go.click()
            result = 0
            while True:
                result = len(self.find_elements_by_xpath('//a[@class="Result1" and text()="Edit"]'))
                if result not in [0, 1]:
                    sleep(10)
                else:
                    break

            # did not find the invoice
            if result == 0:
                continue

            btn_edit = self.find_element_by_xpath('//a[@class="Result1" and text()="Edit"]')
            btn_edit.click()

            btn_info = self.find_element_by_xpath('//a[em[text()="Additional Information"]]')
            btn_info.click()

            inp_image = self.find_element_by_xpath('//input[@name="_obj__IMAGE_URL"]')
            inp_image.clear()
            inp_image.send_keys(self.items[key]['file_link'])

            btn_post = self.find_element_by_xpath('//a[@id="savebuttid"]')
            btn_post.click()
            while True:
                if len(self.find_elements_by_xpath('//a[@class="Result1" and text()="Edit"]')) == 1:
                    self.write_logs('update.log', {key: self.items[key]})
                    break
                else:
                    sleep(10)

        # store cookies to cookie.log
        with open('cookie.log', 'w') as f:
            json.dump(self.driver.get_cookies(), f)
            f.close()

        self.driver.close()

    def load_logs(self, file_name):
        with open(file_name, 'r') as f:
            try:
                items = json.load(f)
            except:
                items = {}
            return items

    def write_logs(self, file_name, item):
        self.logs.update(item)
        with open(file_name, 'w') as f:
            json.dump(self.logs, f)
            f.close()

    def run(self):
        self.logs = self.load_logs('update.log')
        info = self.load_logs('scrapy.log')
        self.items = {key: info[key] for key in info if key not in self.logs}
        logging.info(self.items)
        if not self.items:
            logging.info('No items')
            return

        self.update_intacct()

    def find_element_by_xpath(self, xpath):
        while True:
            try:
                return self.driver.find_element_by_xpath(xpath)
            except:
                sleep(10)
                continue

    def find_element_by_id(self, id):
        while True:
            try:
                return self.driver.find_element_by_id(id)
            except:
                sleep(10)
                continue

    def find_elements_by_xpath(self, xpath):
        while True:
            try:
                return self.driver.find_elements_by_xpath(xpath)
            except:
                sleep(10)
                continue

if __name__ == '__main__':
    cls = Intacct()
    cls.run()